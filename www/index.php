<?php
include 'includes/header.php';
?>

<div id="left-pane">
	<div class="boxed">
		<img src="/img/us.jpg" />
	</div>
</div>

<div id="content" class="boxed">
	<h1>Welcome!</h1>
	<hr />
	
	<p>This is the homepage for the wedding between <strong>Michael Moore</strong> and <strong>Taylor Hamilton</strong>.</p>
	<p>Use this site to get information about the <a href="/wedding.php">wedding</a> and <a href="/reception.php">reception</a>, view our <a href="/registries.php">registries</a>, and <a href="/contact.php">contact</a> us.</p>
	
	<p>Come back after the wedding to see the guestbook and pictures. We hope to see you there!</p>
</div>

<?php
include 'includes/footer.php';
?>
