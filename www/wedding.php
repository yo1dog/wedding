<?php
include 'includes/header.php';
?>

<div id="left-pane">
	<div class="boxed">
		<img src="/img/church.jpg" />
	</div>

	<div class="boxed">
		<img src="/img/weddingMap.png" />
	</div>
</div>

<div id="content" class="boxed">
	<h1>Wedding</h1>
	<hr />
	
	<h3>When</h3>
	<p>May 17th at 2:00 PM<br />
	Please arrive 15 minutes early.</p>
	
	<h3>Where</h3>
	<p><a href="http://maps.apple.com/?f=d&source=s_d&saddr=&daddr=5455+Bee+Cave+Rd&hl=en&geocode=FZsxzgEd2k4r-g&aq=&sll=30.291904,-97.823918&sspn=0.006596,0.008283&vpsrc=6&mra=mr&ie=UTF8&ll=30.289459,-97.824347&spn=0.012803,0.017102&t=m&z=16&iwloc=ddw0" target="_blank">
	St. John Neumann Catholic Church<br />
	5455 Bee Cave Rd<br />
	West Lake Hills, TX 78746</a></p>
	
	<h3>Dress</h3>
	<p>Formal, it is a wedding after all!</p>
	
	<h3>Directions</h3>
	<ol>
		<li>From 360, take the FM 2244/Bee Cave Rd exit and turn onto Bee Cave Rd heading east.</li>
		<li>In 1/2 miles, take the second right heading south-west on Redbud trail.</li>
		<li>In 1/4 miles, you will be in the church's parking lot.</li>
	</ol>
	
	<h3>Rehearsal</h3>
	<p>If you are part of the wedding party, the rehearsal will be the day before the wedding on May 16th at 5:00 PM. They are sticklers for starting on time so please make sure you are at the church before 5:00! Dinner will be provided afterwards. Lodging will be provided for the night of the 16th. <a href="/contact.php">Contact us</a> for details.</p>
</div>

<?php
include 'includes/footer.php';
?>