<?php
include 'includes/header.php';
?>

<div class="boxed">
	<h1>Registries</h1>
	<hr />
	
	<p>We are registed at the following locations:</p>
	<ul>
		<li><a href="/scripts/JCPennyRegistryForward.php" target="_blank">JCPenney</a></li>
		<li><a href="http://www.myregistry.com/wedding-registry/Michael-Moore-Taylor-Hamilton-/596878" target="_blank">Beer Brewing Stuff</a></li>
		<li>... more soon to come!</li>
	</ul>
</div>

<?php
include 'includes/footer.php';
?>
