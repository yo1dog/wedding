<?php
include 'includes/header.php';
?>

<div class="boxed">
	<h1>Oops!</h1>
	<p>Something went wrong.</p>
	<p>Are you looking for information about the <a href="/wedding.php">wedding</a>? The <a href="/reception.php">reception</a>? Our <a href="/registries.php">registries</a>? Or, do you need to <a href="/contact.php">contact us</a>?</p>
</div>

<?php
include 'includes/footer.php';
?>