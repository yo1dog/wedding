<?php
define('CURLE_ABORTED_BY_CALLBACK', 42);

$curl = NULL;
$useMobile = false;

$JCPennyURL       = 'http://www.jcpenney.com/dotcom/jsp/giftregistry/gifterGiftList/gifterGiftList.jsp?regId=60043193&sort=category&filterBy=viewAll&view=list&registryType=W';
$JCPennyMobileURL =    'http://m.jcpenney.com/mobile/jsp/giftregistry/mobile/gifter/gifterGiftList.jsp?regId=60043193&sort=category&filterBy=viewAll&view=list&registryType=W';


// get the request headers
$assocHeaders = getallheaders();

if ($assocHeaders === false)
	error_log('Unable to get headers.');
else
{
	// convert assoc array of headers to list array
	$headers = array();
	
	foreach ($assocHeaders as $name => $value)
	{
		if (strtolower($name) !== 'host')
			array_push($headers, $name . ': ' . $value);
	}
	
	
	// foward the client's headers to the non-mobile URL and see if we get back a non-200
	$curl = curl_init();
	
	if ($curl === false)
                error_log('cURL failed to init with error ' . curl_errno($curl) . ': ' . curl_error($curl));
	
        else
	{
		curl_setopt($curl, CURLOPT_URL           , $JCPennyURL);//'http://wedding.awesomebox.net/test.php');
		curl_setopt($curl, CURLOPT_HTTPHEADER    , $headers);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_HEADER        , true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		
		// We only care about the status code, not the body. But using CURLOPT_NOBODY tells cURL to use the HEADER
		// HTTP verb, which JCPenny's site does not support and always returns a 500. So instead, we use
		// CURLOPT_PROGRESSFUNCTION with a small buffer size and stop the download as soon as we have downloaded
		// enough of the header to parse the status code.
		curl_setopt($curl, CURLOPT_BUFFERSIZE, 16);
		curl_setopt($curl, CURLOPT_NOPROGRESS, false);
		curl_setopt($curl, CURLOPT_PROGRESSFUNCTION,
			function ($downloadSize, $downloaded, $uploadSize, $uploaded)
			{
				global $curl;
				
				// if we have the status code, return non-zero to stop the download.
				// Note that this results in an error being returned from curl_exec
				// 42 CURLE_ABORTED_BY_CALLBACK
				return curl_getinfo($curl, CURLINFO_HTTP_CODE) > 0 ? 1 : 0;
			});
		
		$result = curl_exec($curl);
		$errorNum = curl_errno($curl);		
		
		if ($result === false && $errorNum !== CURLE_ABORTED_BY_CALLBACK)
			error_log('cURL failed to exec with error ' . $errorNum . ': ' . curl_error($curl));
		
		else
			$useMobile = curl_getinfo($curl, CURLINFO_HTTP_CODE) !== 200;
		
		curl_close($curl);
	}
}


// redirect the user to the correct site
header('Location: ' . ($useMobile ? $JCPennyMobileURL : $JCPennyURL), true, 301);
?>
