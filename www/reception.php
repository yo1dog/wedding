<?php
include 'includes/header.php';
?>

<div id="left-pane">
	<div class="boxed">
		<img src="/img/CLOTLFront.jpg" />
	</div>

	<div class="boxed">
		<img src="/img/receptionMap.png" />
	</div>
</div>

<div id="content" class="boxed">
	<h1>Reception</h1>
	<hr />
	
	<h3>When</h3>
	<p>May 17th at 4:00 PM</p>
	
	<h3>Where</h3>
	<p><a href="http://maps.apple.com/?f=d&source=s_d&saddr=&daddr=The+County+Line,+5204+Ranch+Road+2222,+Austin,+TX+78731&geocode=FZs2zwEdCukr-imj0HLFzDRbhjFZ4OpdNn--uw&aq=&sll=30.361767,-97.784071&sspn=0.025587,0.034204&vpsrc=6&hl=en&mra=ls&ie=UTF8&t=m&ll=30.35836,-97.786303&spn=0.012794,0.017102&z=16&iwloc=ddw1" target="_blank">
	The County Line on The Lake<br />
	5204 F.M. 2222<br />
	Austin, TX 78731</a><br />
	<br />
	(512) 346-3664</p>
	
	<h3>What</h3>
	<p>BBQ and drinks on us!</p>
	
	<h3>Dress</h3>
	<p>Formal, casual, whatever you are comfortable in. It's just BBQ after all!</p>
	
	<h3>Directions</h3>
	<p><strong>Not</strong> County Line on the Hill off Bee Cave Rd. The correct place is further north off of 360 (confusing, we know).</p>
	
	<ol>
		<li>From the church, head north-east out of the parking lot on Redbud trail towards Bee Cave Rd.</li>
		<li>In 1/4 miles, take the first left onto Bee Cave Rd.</li>
		<li>In 1/2 miles, take a right onto 360/Capital of Texas Highway heading north.</li>
		<li>In 5 miles, take exit FM 2222 and turn right heading east on FM 2222.</li>
		<li>In 1/4 miles, the restaurant will be on the right.</li>
	</ol>
</div>

<?php
include 'includes/footer.php';
?>
