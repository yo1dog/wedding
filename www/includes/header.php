<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Mike and Taylor's Wedding</title>
		<link rel="stylesheet" href="/css/style.css" />
		
		<meta name="viewport" content="initial-scale=0.6">
		<meta name="format-detection" content="date=yes">
		
		<script type="text/javascript">
			var isSmall = false;
			
			function init()
			{
				var leftPane = document.getElementById("left-pane");
				var bottomPane = document.getElementById("bottom-pane");
				if (leftPane && bottomPane)
					bottomPane.innerHTML = leftPane.innerHTML;
				
				checkSize();
			}
			
			function checkSize()
			{
				var width;
				
				if (self.innerHeight)
					width = self.innerWidth;
				else if (document.documentElement && document.documentElement.clientWidth)
					width = document.documentElement.clientWidth;
				else if (document.body)
					width = document.body.clientWidth;
				
				if (width < 840)
				{
					if (!isSmall)
					{
						var leftPane = document.getElementById("left-pane");
						if (leftPane)
							leftPane.style.display = "none";
						
						var bottomPane = document.getElementById("bottom-pane");
						if (bottomPane)
							bottomPane.style.display = "block";
						
						document.getElementById("main").style.width = "494px";
						
						isSmall = true;
					}
				}
				else
				{
					if (isSmall)
					{
						var leftPane = document.getElementById("left-pane");						
						if (leftPane)
							leftPane.style.display = "inline-block";
												
						var bottomPane = document.getElementById("bottom-pane");
						if (bottomPane)
							bottomPane.style.display = "none";
						
						document.getElementById("main").style.width = "800px";
						
						isSmall = false;
					}
				}
			}
			
			window.onresize = checkSize;
		</script>
	</head>
	<body onLoad="init();">
		<div id="main">
			<div id="logo" class="boxed"><a href="/">Mike & Taylor</a></div>
			<div id="menu">
				<a href="/wedding.php">Wedding</a>
				&nbsp;&bull;&nbsp;
				<a href="/reception.php">Reception</a>
				&nbsp;&bull;&nbsp;
				<a href="/registries.php">Registries</a>
				&nbsp;&bull;&nbsp;
				<a href="/contact.php">Contact</a>
			</div>
