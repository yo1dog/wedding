<?php
include 'includes/header.php';
?>

<div class="boxed">
	<h1>Contact</h1>
	<hr />
	
	<h3>Mike</h3>
	<p>(214) 616-8895<br />
	<a href="mailto:mike@devmike.net">mike@devmike.net</a></p>
	
	<h3>Taylor</h3>
	<p>(214) 793-6579<br />
	<a href="mailto:taylorhamilton009@gmail.com">taylorhamilton009@gmail.com</a></p>
	
	<h3>Kim Moore</h3>
	<p>(214) 679-6515<br />
	<a href="mailto:kimmoore@swbell.net">kimmoore@swbell.net</a></p>
</div>

<?php
include 'includes/footer.php';
?>
